var express = require('express');
var app = express();
const path = require('path');
const generatePassword = require('password-generator');
const bodyParser = require('body-parser');
const port = process.env.PORT || 5000;
//var birds = require('./router/birds');
let routeInsert = require('./router/routesInsert');
let routeSelect = require('./router/routeSelect');
let routeUpdate = require('./router/routeUpdate');
let routeDelete = require('./router/routeDelete');

// Serve static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

// Put all API endpoints under '/api'
app.get('/api/passwords', (req, res, next) => {
  const count = 5;
  const passwords = Array.from(Array(count).keys()).map(i =>
    generatePassword(12, false)
  )
  res.json(passwords);  
});




app.use('/insert', routeInsert);
app.use('/select', routeSelect);
app.use('/update', routeUpdate);
app.use('/delete', routeDelete);

// The "catchall" handler: for any request that doesn't
// match one above, send back React's index.html file.
app.get('*', (req, res, next) => {
  res.sendFile(path.join(__dirname+'/client/build/index.html'));
  next()
});


app.listen(port);

console.log(`Password generator listening on ${port}`);
