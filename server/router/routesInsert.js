let express = require('express');
let router = express.Router();
let bodyParser = require('body-parser');
let jsonParser = bodyParser.json();
let pool = require('./sql.js');
let promisePool = pool.promise();

// Adding one user, can be the same Email
router.post('/user', jsonParser ,async  (req, res) => {
  let userData = " ( '"+ req.body.email +"', '"+ req.body.haslo +"', '" +req.body.imie + "', '"+req.body.nazwisko + "', '"+ req.body.numer + "', '" + req.body.wlasciciel + "', '" +req.body.wynajmujacy + "' );" ;
  let userInsert = "INSERT INTO `dn5oiatglajltxzf`.`uzytkownicy` (`Email`, `Haslo`, `Imie`, `Nazwisko`, `Nr_telefonu`, `Wlasciciel`, `Wynajmujacy`) VALUES " +userData;  
  

      pool.query(userInsert, function (error, results, fields) {
       if (error){
        res.json("error")
       }      
        else{
          res.json({answer:"add user"});
        }
      }); 


  
   
});

// Adding one house, you must posses your ID to use this post
router.post('/house', jsonParser , async (req, res) => {  
  let houseData = " ( '"+ req.body.nazwa +"', '"+ req.body.metraz +"', '" +req.body.ulica + "',  '" +req.body.nrUlicy + "', '"+req.body.nrMieszkania + "', '"+ req.body.miasto + "', '" + req.body.opis + "', '" +req.body.userID + "' );" ;
  let houseInsert = "INSERT INTO `mieszkania` (`Nazwa`, `Metraz`, `Ulica`,`Nr_Ulicy` , `Nr_Mieszkania`, `Miasto`, `Opis`, `ownerID`) VALUES " +houseData;  
  

      pool.query(houseInsert, function (error, results, fields) {
       if (error){
        res.json("error")
       }      
        else{
          res.json({id : results['insertId']});
        }
      });  


  
   
});


//Adding furnishing, you have to posses houseID to use this post
router.post('/furnishing', jsonParser , async (req, res) => {  
  let furData = " ( '"+ req.body.nazwa +"', '"+ req.body.ilosc +"', '"  +req.body.houseID + "' );" ;
  let furInsert = "INSERT INTO `Wyposazenie` (`Nazwa`, `Ilosc`, `houseID`) VALUES " +furData;  
  

      pool.query(furInsert, function (error, results, fields) {
       if (error){
        res.json("error")
       }      
        else{
          res.json({answer : "furnishing data changed"});
        }
      });  


  
   
});


//Adding agreement, you check id of added users with Email(/select/email), you have to posses houseID to use this post
router.post('/agreement', jsonParser , async (req, res) => {  
  let emailSelect = "Select `idUzytkownicy` FROM `uzytkownicy` WHERE `Email` =  '"+ req.body.Email +"' ; ";  
  let emailInfo;

  [emailInfo, rows] = await promisePool.query(emailSelect);
  let emailId = await emailInfo[0]["idUzytkownicy"]

  let agreData = " ( '"+ req.body.nazwa +"', '"+ req.body.dataOd +"', '" +req.body.dataDo + "', '"+ req.body.nrMieszkancow + "', '"+ req.body.kwotaMiesieczna + "', '" + req.body.terminPlatnosci + "', '" + req.body.kaucja + "', '" + req.body.okresWypowiedzenia + "', '" + req.body.houseID + "', '" + await emailId + "' );" ;
  let agreInsert = "INSERT INTO `umowy` (`Nazwa`, `Data_Od`, `Data_Do`, `Nr_Mieszkancow`, `Kwota_Miesieczna`, `Termin_Platnosci`, `Kaucja`, `Okres_Wypowiedzenia`, `houseID`, `wynajmujacyID`) VALUES " + await agreData;
  let agreID;
  
  pool.query(agreInsert, function (error, results, fields) {
    if (error){
     res.json("error")
    }      
     else{
       res.json({answer:"add agreement"});
     }
   }); 


  
   
});

//Adding visit, you have to posses houseID to use this post
router.post('/visit', jsonParser , async (req, res) => {  
  let visData = " ( '"+ req.body.nazwa +"', '"+ req.body.tresc +"', '" +req.body.dataWizytacji + "', '"+ req.body.dataDodania + "', '"+ req.body.houseID + "' );" ;
  let visInsert = "INSERT INTO `wizytacje` (`Nazwa`, `Tresc`, `Data_Wizytacji`, `Data_Dodania`,  `houseID`) VALUES " +visData;
  

      pool.query(visInsert, function (error, results, fields) {
       if (error){
        res.json("error")
       }      
        else{
          res.json({answer:"add visit"});
        }
      });  


  
   
});

//Adding damage, you have to posses houseID
router.post('/damage', jsonParser , async (req, res) => {  
  let damData = " ( '"+ req.body.nazwa +"', '"+ req.body.dataAwarii +"', '" +req.body.opisAwarii + "', '"+ req.body.akceptacjaAwarii + "', '"+ req.body.houseID + "', '" + req.body.userID + "' );" ;
  let damInsert = "INSERT INTO `awarie` (`Nazwa_Awarii`, `Data_Awarii`, `Opis_Awarii`, `Akceptacja_Awarii`, `houseID`, `userID`) VALUES " +damData;
  

      pool.query(damInsert, function (error, results, fields) {
       if (error){
        res.json("error")
       }      
        else{
          res.json({answer:"add demage"});
        }
      });  


  
   
});

// Adding event, you have to posses agreementID
router.post('/event', jsonParser , async (req, res) => {  
  let eveData = " ( '"+ req.body.typ +"', '"+ req.body.nazwa +"', '" +req.body.agreementID + "' );" ;
  let eveInsert = "INSERT INTO `zdarzenia` (`Typ`, `Nazwa`, `agreementID`) VALUES " +eveData;
  

      pool.query(eveInsert, function (error, results, fields) {
       if (error){
        res.json("error")
       }      
        else{
          res.json({answer:"add event"});
        }
      });  


  
   
});

// Adding message, you have to posses userID and eventID 
router.post('/message', jsonParser , async (req, res) => {  
  let mesData = " ( '"+ req.body.tresc +"', '"+ req.body.agreID + "', '"+ req.body.userID +  "' );" ;
  let mesInsert = "INSERT INTO `wiadomosci` (`Tresc`, `agreementID`, `userID`) VALUES " +mesData;
  

      pool.query(mesInsert, function (error, results, fields) {
       if (error){
        res.json("error")
       }      
        else{
          res.json({answer:"add message"});
        }
      });  


  
   
});


//Adding payment, you have to posses agreementID
router.post('/payment', jsonParser , async (req, res) => {  
  let payData = " ( '"+ req.body.nazwa +"', '"+ req.body.kwota +"', '" +req.body.dataDodania + "', '"+ req.body.agreementID + "' );" ;
  let payInsert = "INSERT INTO `oplaty` (`Nazwa`, `Kwota`, `Data_Dodania`, `agreementID`) VALUES " +payData;
  

      pool.query(payInsert, function (error, results, fields) {
       if (error){
        res.json("error")
       }      
        else{
          res.json({answer:"add payment"});
        }
      });  


  
   
});


module.exports = router;