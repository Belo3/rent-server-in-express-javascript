let express = require('express');
let router = express.Router();
let bodyParser = require('body-parser');
let jsonParser = bodyParser.json();
let pool = require('./sql.js');
let promisePool = pool.promise();

// Updates user, provide all parameters, all will be changed, you can provide same parameters.
router.post('/userInfo', jsonParser, async (req, res, next) => {
    let userUpdate = "UPDATE `Uzytkownicy` SET  `Imie` = '" + req.body.imie + "', `Nazwisko`= '" + req.body.nazwisko + "', `Nr_telefonu` = '" + req.body.numer + "' where `idUzytkownicy` = '" + req.body.userID + "' ;   " ;
    let userCheck = "SELECT `uzytkownicy`.`idUzytkownicy`,  `uzytkownicy`.`Email`,  `uzytkownicy`.`Imie`,  `uzytkownicy`.`Nazwisko`,  `uzytkownicy`.`Nr_telefonu`,  `uzytkownicy`.`Wlasciciel`,  `uzytkownicy`.`Wynajmujacy` FROM `dn5oiatglajltxzf`.`uzytkownicy` WHERE `idUzytkownicy`='" + req.body.userID + "' ;";  
  
    let userInfo;  
    await promisePool.query(userUpdate);
    [userInfo, rows] = await promisePool.query(userCheck);
    res.json({"id": userInfo});

});


router.post('/userPass', jsonParser, async (req, res, next) => {
    let userPassword = "UPDATE `Uzytkownicy` SET `Haslo` = '" + req.body.haslo + "' where `idUzytkownicy` = '" + req.body.userID + "' ;   " ;

    pool.query(userPassword, function (error, results, fields) {
        if (error) {
            res.json('error');
        }
        else{
            res.json('User Data Changed'); 
        }
   });
});




router.post('/userRole', jsonParser, async (req, res, next) => {
    let userRoles = "UPDATE `Uzytkownicy` SET `Wlasciciel` = '" + req.body.wlasciciel + "', `Wynajmujacy` = '" + req.body.wynajmujacy + "' where `idUzytkownicy` = '" + req.body.userID + "' ;   " ;

    pool.query(userRoles, function (error, results, fields) {
        if (error) {
            res.json('error');
        }
        else{
            res.json({Update:'User Data Changed'}); 
        }
   });
}); 



// Update house, provide all parameters, all will be changed, you can provide same parameters.
router.post('/house', jsonParser, async (req, res, next) => {
    let  houseUpdate = "UPDATE `mieszkania` SET `Nazwa` = '" + req.body.nazwa + "', `Metraz` = '" + req.body.metraz + "', `Ulica`= '" + req.body.ulica + "', `Nr_Ulicy` = '" + req.body.nrUlicy + "', `Nr_Mieszkania` = '" + req.body.nrMieszkania + "', `Miasto` = '" + req.body.miasto + "', `Opis` = '" + req.body.opis + "' where `idMieszkania` = '" + req.body.houseID + "' ;   " ;

    pool.query(houseUpdate, function (error, results, fields) {
        if (error) {
            res.json('error');
        }
        else{
            res.json({Update:'House Data Changed'}); 
        }
   });
  });

// Update furnitures, for owner
router.post('/furnishing', jsonParser, async (req, res, next) => {
    let  furUpdate = "UPDATE `Wyposazenie` SET  `Ilosc` = '" + req.body.ilosc + "' where `idWyposażenie` = '" + req.body.furID + "' ;   " ;

    pool.query(furUpdate, function (error, results, fields) {
        if (error) {
            res.json('error');
        }
        else{
            res.json({Update:'Furnishing Data Changed'}); 
        }
   });
  }); 

// Update payment, for user thats paying
router.post('/paymentPaying', jsonParser, async (req, res, next) => {
    let payUpdate = "UPDATE `Oplaty` SET `Kwota_Zaplacona` = '" + req.body.kwota + "', `Data_zaplacenia` = '" + req.body.dataZaplacenia + "' where `idOplaty` = '" + req.body.paymentID + "' ;   " ;

    pool.query(payUpdate, function (error, results, fields) {
        if (error) {
            res.json('error');
        }
        else{
            res.json({Update:'Payment Data Changed'}); 
        }
   });
  });

// Update agreement, for activation
router.post('/agreementActivation', jsonParser, async (req, res, next) => {
    let argUpdate = "UPDATE `Umowy` SET `Aktywna` =  '"+ req.body.aktywna +"' where `idUmowy` = '" + req.body.agreementID + "' ;   " ;

    pool.query(argUpdate, function (error, results, fields) {
        if (error) {
            res.json('error');
        }
        else{
            res.json({Update:'Agrement active changed'}); 
        }
   });
  });

// Update damage, for accpeting by owner  
router.post('/damage', jsonParser, async (req, res, next) => {
    let damAcc = "call Owner_Damage_Acc("+ req.body.damageID +");";   
    let damRes = "call Owner_Damage_Res("+ req.body.damageID +");";
    
    if(req.body.choice==0){await promisePool.query(damAcc)}
    else if (req.body.choice==1){await promisePool.query(damRes)}
    res.json({Update:'Accepted change'});
  });


router.post('/notifyRead', jsonParser, async (req, res, next) => {
    
    
    let notify_readD = "call dn5oiatglajltxzf.Read_damage("+ req.body.notifyID +");";   
    let notify_readV = "call dn5oiatglajltxzf.Read_visit("+ req.body.notifyID +");";
    

    if(req.body.choice==1){await promisePool.query(notify_readD)}
    else if (req.body.choice==0){await promisePool.query(notify_readV)}
    res.json({Update:'Accepted change'});
  });



module.exports = router;