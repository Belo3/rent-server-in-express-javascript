let express = require('express');
let router = express.Router();
let bodyParser = require('body-parser');
let jsonParser = bodyParser.json();
let pool = require('./sql.js');
let promisePool = pool.promise();

// fetch list of all users
router.post('/getAllUsers', async (req, res, next) => {
  

  pool.query('SELECT * from `dn5oiatglajltxzf`.`uzytkownicy`', function (error, results, fields) {
   if (error) {res.json('error')}
   res.json({results :results}); 
 });
});

//fetch list of all houses
router.post('/getAllHouses', async (req, res, next) => {
  

  pool.query('SELECT * from `dn5oiatglajltxzf`.`mieszkania`', function (error, results, fields) {
   if (error) {res.json('error')}
   res.json({results :results}); 
 });
});






// logs in with email and pass
router.post('/login', jsonParser ,async  (req, res) => {  
  let userCheck = "SELECT `uzytkownicy`.`idUzytkownicy`,  `uzytkownicy`.`Email`,  `uzytkownicy`.`Imie`,  `uzytkownicy`.`Nazwisko`,  `uzytkownicy`.`Nr_telefonu`,  `uzytkownicy`.`Wlasciciel`,  `uzytkownicy`.`Wynajmujacy` FROM `dn5oiatglajltxzf`.`uzytkownicy` WHERE `Email`='" +req.body.email + "' AND `Haslo`='" +req.body.haslo + "' ;";  
  

      pool.query(userCheck, function (error, results, fields) {
       if (results==""){
        res.json("Wrong credentials")
       }      
        else{
          res.json({"id": results});
        }
      }); 


  
   
});





//returns all houses for a user
router.post('/house', jsonParser ,async  (req, res) => {  
    let houseSelect = "SELECT * FROM `server_house_notify_for_owner` where ownerID = "+ req.body.userID +";";  
    
  
    pool.query(houseSelect, function (error, results, fields) {
        if (error){
          res.json("error")
        }      
         else{
          res.json({house: results});
        }
       });  
  
  
    
     
});

//returns all houses for a Rentie
router.post('/houseForRent', jsonParser ,async  (req, res) => {  
  let houseRentSelect = "SELECT * FROM `server_house_notify_for_rentie` WHERE `wynajmujacyID` = " +req.body.userID+ " ; "; 

  pool.query(houseRentSelect, function (error, results, fields) {
      if (error){
        res.json("error")
      }      
       else{
        res.json({house: results});
      }
     });  


  
   
});

// returns all furnishings for a user
router.post('/furnishing', jsonParser ,async  (req, res) => {  
    let furSelect = "SELECT `wyposazenie`.`*` FROM `wyposazenie` WHERE `houseID` = "+ req.body.houseID +" ;";  
    
  
    pool.query(furSelect, function (error, results, fields) {
        if (error){
         res.json("error")
        }      
         else{
           res.json({furnishing: results});
         }
       });  
  
  
    
     
});

// returns all furnishings for a rentie
router.post('/furnishingForRent', jsonParser ,async  (req, res) => {  
  let furRentSelect = "SELECT `wyposazenie`.`*` FROM `wyposazenie`, `umowy` WHERE `umowy`.`houseID` = `wyposazenie`.`houseID` AND `wynajmujacyID` = "+ req.body.userID +" ;";  
  

  pool.query(furRentSelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }      
       else{
         res.json({furnishing: results});
       }
     });  


  
   
});

// returns all agreements for house, this is for the owner
router.post('/agreement', jsonParser ,async  (req, res) => {  
  let agreSelect = " SELECT * FROM `server_agreement_and_balance` WHERE `ownerID` = " + req.body.userID + " ; ";  
  

  pool.query(agreSelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }
      else if (results==""){
          res.json("no agrement");
      }      
       else{             
         res.json({agreement: results});
       }
     });  


  
   
});

// returns all agreements for user, this is for renting one
router.post('/agreementForRent', jsonParser ,async  (req, res) => {  
  let agreRentSelect = "SELECT * FROM `server_agreement_and_balance` WHERE `wynajmujacyID`="+ req.body.userID +"  ;";  
  

  pool.query(agreRentSelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }
      else if (results==""){
          res.json("no agrement");
      }      
       else{             
         res.json({agreement: results});
       }
     });  


  
   
});

// returns userID for email
router.post('/email', jsonParser ,async  (req, res) => {  
    let emailSelect = "SELECT `idUzytkownicy` FROM `uzytkownicy` WHERE `Email`='" +req.body.email+ "' AND `Wynajmujacy` = '1' ;";  
    
  
    pool.query(emailSelect, function (error, results, fields) {
        if (error){
         res.json("error")
        }
        else if (results==""){
            res.json("Wrong email");
        }      
         else{             
           res.json({email: results});
         }
       });  
  
  
    
     
});

// returns all visits for owner
router.post('/visit', jsonParser ,async  (req, res) => {  
  let visSelect = "SELECT `wizytacje`.`*` FROM `wizytacje` , `mieszkania` WHERE `ownerID` = "+ req.body.userID +" AND `idMieszkania` = `houseID` AND  (NOW() <= `wizytacje`.`Data_Wizytacji`);";  
  

  pool.query(visSelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }
      else if (results==""){
          res.json("no visits");
      }      
       else{             
         res.json({visits: results});
       }
     });  


  
   
});

// returns all visits for rentie
router.post('/visitForRent', jsonParser ,async  (req, res) => {  
  let visRentSelect = "SELECT `wizytacje`.`*` FROM `wizytacje`, `umowy` WHERE `wynajmujacyID` = "+ req.body.userID +" AND `umowy`.`houseID` = `wizytacje`.`houseID` AND  (NOW() <= `wizytacje`.`Data_Wizytacji`);";  
  

  pool.query(visRentSelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }
      else if (results==""){
          res.json("no visits");
      }      
       else{             
         res.json({visits: results});
       }
     });  


  
   
});


//Selects All damage for owner
router.post('/damage', jsonParser ,async  (req, res) => {  
  let damSelect = "SELECT `awarie`.`*` FROM `awarie`, `mieszkania` WHERE `ownerID` = "+ req.body.userID +" AND `idMieszkania` = `awarie`.`houseID` AND `Rozwiazanie_Awarii` = 0;";  
  

  pool.query(damSelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }
      else if (results==""){
          res.json("no damage");
      }      
       else{             
         res.json({damage: results});
       }
     });  


  
   
});

//Selects All damage for rentie
router.post('/damageForRent', jsonParser ,async  (req, res) => {  
  let damRentSelect = "SELECT `awarie`.`*` FROM `awarie`, `umowy` WHERE `wynajmujacyID` = "+ req.body.userID +" AND `umowy`.`houseID` = `awarie`.`houseID` AND `Rozwiazanie_Awarii` = 0;";  
  

  pool.query(damRentSelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }
      else if (results==""){
          res.json("no damage");
      }      
       else{             
         res.json({damage: results});
       }
     });  


  
   
});

// returns all events for owner
router.post('/event', jsonParser ,async  (req, res) => {  
  let eveSelect = "SELECT `zdarzenia`.`*` FROM `zdarzenia`, `umowy`, `mieszkania` WHERE `ownerID` = "+ req.body.userID +" AND `idMieszkania` = `umowy`.`houseID` AND `idUmowy`= `agreementID`;";  
  

  pool.query(eveSelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }
      else if (results==""){
          res.json("no event");
      }      
       else{             
         res.json({event: results});
       }
     });  


  
   
});

// returns all events for rentie
router.post('/eventForRent', jsonParser ,async  (req, res) => {  
  let eveRentSelect = "SELECT `zdarzenia`.`*` FROM `zdarzenia`, `umowy` WHERE `wynajmujacyID` = "+ req.body.userID +" AND `idUmowy`= `agreementID`;";  
  

  pool.query(eveRentSelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }
      else if (results==""){
          res.json("no event");
      }      
       else{             
         res.json({event: results});
       }
     });  


  
   
});



// returns all payments for Owners houses
router.post('/payment', jsonParser ,async  (req, res) => {  
  paySelect = "SELECT `oplaty`.`*` FROM `umowy`, `mieszkania`, `oplaty` where `ownerID` =  "+ req.body.userID +" AND `idMieszkania` = `houseID`  AND `idUmowy` = `agreementID` ;";  
  

  pool.query(paySelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }
      else if (results==""){
          res.json("no payment");
      }      
       else{             
         res.json({payment: results});
       }
     });  


  
   
});

// returns all payments for rentie agreements
router.post('/paymentForRent', jsonParser ,async  (req, res) => {  
  let payRentSelect = "SELECT `oplaty`.`*` FROM `umowy`, `oplaty` where `wynajmujacyID` = "+ req.body.userID +"  AND `idUmowy` = `agreementID` ;";  
  

  pool.query(payRentSelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }
      else if (results==""){
          res.json("no payment");
      }      
       else{             
         res.json({payment: results});
       }
     });  


  
   
});

// returns email for email
router.post('/forgotPass', jsonParser ,async  (req, res) => {  
  let emailSelect = "SELECT `Email` FROM `uzytkownicy` WHERE `Email`='" +req.body.email+ "' ;";  
  

  pool.query(emailSelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }
      else if (results==""){
          res.json("Wrong email");
      }      
       else{             
         res.json({email: results});
       }
     });  


  
   
});


router.post('/notificationsForRent', jsonParser ,async  (req, res) => {  
  let notDamRent = "SELECT `notifyDamageID`, `Opis`, `Czas`, `damageID` From `server_notify_damage` WHERE `wynajmujacyID` = "+ req.body.userID  +" ;";  
  let notDamInfo;
  let notVisRent = "SELECT `notifyVisitID`, `Opis`, `Czas`, `visitID` FROM `server_notify_visit` WHERE `wynajmujacyID`="+ req.body.userID +"  ;";
  let notVisInfo;
  let notMessRent = "SELECT * FROM `view_new_messages_for_rentie`  WHERE `wynajmujacyID`="+ req.body.userID +"  ;"; 
  let notMessInfo;
  let notAgreRent = "SELECT * FROM view_notify_agre WHERE `wynajmujacyID`="+ req.body.userID +"   ;";
  let notAgreInfo;

  [notDamInfo, rows] = await promisePool.query(notDamRent);
  [notVisInfo, rows] = await promisePool.query(notVisRent);
  [notMessInfo, rows] = await promisePool.query(notMessRent);
  [notAgreInfo, rows] = await promisePool.query(notAgreRent);

  res.json({NotifyAgre:notAgreInfo,NotifyDamage : notDamInfo, NotifyVisit: notVisInfo,NorifyMessage: notMessInfo});
   
});

router.post('/notificationsForOwner', jsonParser ,async  (req, res) => {  
  let notDamRent = "SELECT `Opis`,`idAwarie`,`Data_Awarii` From `server_notify_damage_owner` WHERE `ownerID` = "+ req.body.userID  +" ;";  
  let notDamInfo;
  let notMessRent = "SELECT * FROM `view_new_messages_for_owner`  WHERE `ownerID`="+ req.body.userID +"  ;"; 
  let notMessInfo;
  

  [notDamInfo, rows] = await promisePool.query(notDamRent);
  [notMessInfo, rows] = await promisePool.query(notMessRent);
  

  res.json({NotifyDamage : notDamInfo, NorifyMessage: notMessInfo});
   
});



router.post('/messageAgeement', jsonParser ,async  (req, res) => { 

      let mess = "Select * From `server_message` WHERE `agreementID` = "+ req.body.agreID +";";
      let messInfo;
      let mesRead = "call Message_Read("+ req.body.agreID +", "+ req.body.userID +");";     
      
      
          
      [messInfo, rows] = await promisePool.query(mess);
      await promisePool.query(mesRead);  
    
      res.json({message : messInfo});
       
    });
    

// returns all agreements for house, this is for the owner
router.post('/agreementByHouse', jsonParser ,async  (req, res) => {  
  let agreSelectHouse = " SELECT `server_agreement_and_balance`.`*`, `Imie`, `Nazwisko` FROM `server_agreement_and_balance` JOIN `uzytkownicy` ON `wynajmujacyID` = `idUzytkownicy` WHERE `houseID` = " + req.body.houseID + " ; ";  
  let agreInfo;

  

  [agreInfo, rows] = await promisePool.query(agreSelectHouse);
  
  res.json({  agreement: agreInfo});

  
   
});


router.post('/agreementOne', jsonParser ,async  (req, res) => {  
  let agreSelectHouse = " SELECT * FROM `server_agreement_and_balance` WHERE `idUmowy` = " + req.body.agreID + " ; ";  
  let agreInfo;

  

  [agreInfo, rows] = await promisePool.query(agreSelectHouse);
  
  res.json({  agreement: agreInfo});

  
   
});

router.post('/eventByAgreement', jsonParser ,async  (req, res) => {  
  let eveSelectOne = "Select `idZdarzenie` From `zdarzenia` Join `umowy` ON `agreementID` = `idUmowy` where `idUmowy` =" + req.body.agreID + ";";  
  

  pool.query(eveSelectOne, function (error, results, fields) {
      if (error){
       res.json("error")
      }     
       else{             
         res.json({event: results});
       }
     });  


  
   
});



router.post('/paymentByAgreement', jsonParser ,async  (req, res) => {  
  paySelect = "SELECT `oplaty`.`idOplaty`,  `oplaty`.`Nazwa`,  `oplaty`.`Kwota`, DATE_FORMAT(`oplaty`.`Data_Dodania`, '%M %d %Y') AS `Data_Dodania` ,  `oplaty`.`Kwota_Zaplacona`, DATE_FORMAT(`oplaty`.`Data_Zaplacenia`, '%M %d %Y') AS `Data_Zaplacenia`,  `oplaty`.`agreementID` FROM  `oplaty` where `agreementID` = "+ req.body.agreID +" AND `oplaty`.`Kwota` <> `oplaty`.`Kwota_Zaplacona`;";  
  

  pool.query(paySelect, function (error, results, fields) {
      if (error){
       res.json("error")
      }     
       else{             
         res.json({payment: results});
       }
     });  


  
   
});


router.post('/visitByHouse', jsonParser ,async  (req, res) => {  
  let visSelectByHouse = "SELECT `wizytacje`.`idWizyt`,  `wizytacje`.`Nazwa`,  `wizytacje`.`Tresc`, date_format(`wizytacje`.`Data_Wizytacji`, '%T %M %d %Y ') AS `dateVisit`, DATE_FORMAT(`wizytacje`.`Data_Dodania`, '%M %d %Y') AS `dateFrom`, `wizytacje`.`houseID` FROM `wizytacje`  WHERE `houseID` = "+ req.body.houseID+" AND (NOW() <= `wizytacje`.`Data_Wizytacji`);";  
  

  pool.query(visSelectByHouse, function (error, results, fields) {
      if (error){
       res.json("error")
      }    
       else{             
         res.json({visits: results});
       }
     });  


  
   
});
  
router.post('/damageSelectByhouseID', jsonParser ,async  (req, res) => {  
  let damageSelectByhouseID = "Select `server_damage_houseid`.`*` From `server_damage_houseid` where `houseID` =" + req.body.houseID + ";";  
  

  pool.query(damageSelectByhouseID, function (error, results, fields) {
      if (error){
       res.json("error")
      }     
       else{             
         res.json({damage: results});
       }
     });  


  
   
});

router.post('/houseAgrementForRent', jsonParser ,async  (req, res) => {  
  let houseRentSelect = "SELECT * FROM `server_house_notify_agreement_for_rentie` WHERE `wynajmujacyID` = " +req.body.userID+ " ; "; 

  pool.query(houseRentSelect, function (error, results, fields) {
      if (error){
        res.json("error")
      }      
       else{
        res.json({house: results});
      }
     });  


  
   
});



router.post('/fetchAllOneHouse', jsonParser ,async  (req, res) => {
  let furSelectH = "SELECT `wyposazenie`.`*` FROM `wyposazenie` WHERE `houseID` = "+ req.body.houseID +"  ;";  
  let furInfoH;
  let agreSelectH = " SELECT `server_agreement_and_balance`.`*`, `Imie`, `Nazwisko` FROM `server_agreement_and_balance` JOIN `uzytkownicy` ON `wynajmujacyID` = `idUzytkownicy` WHERE `houseID` = " + req.body.houseID + " ; ";  
  let agreInfoH;
  let visSelectH = "SELECT `wizytacje`.`idWizyt`,  `wizytacje`.`Nazwa`,  `wizytacje`.`Tresc`, date_format(`wizytacje`.`Data_Wizytacji`, '%T %M %d %Y ') AS `Data_Wizytacji`, DATE_FORMAT(`wizytacje`.`Data_Dodania`, '%M %d %Y') AS `Data_Dodania`, `wizytacje`.`houseID` FROM `wizytacje` WHERE `houseID` = "+ req.body.houseID +" ;";  
  let visInfoH;
  let damageSelectAsync = "Select `server_damage_houseid`.`*` From `server_damage_houseid` where `houseID` =" + req.body.houseID + ";";
  let damageInfoH;

  [furInfoH, rows] = await promisePool.query(furSelectH);
  [agreInfoH, rows] = await promisePool.query(agreSelectH);
  [visInfoH, rows] = await promisePool.query(visSelectH);
  [damageInfoH, rows] = await promisePool.query(damageSelectAsync); 
  
  res.json({ furnishing: furInfoH, agreement: agreInfoH, visits: visInfoH, damage: damageInfoH});
   
});

router.post('/fetchAllOneHouseRentie', jsonParser ,async  (req, res) => {
  let agreSelectH = " SELECT `server_agreement_and_balance`.`*`, `Imie`, `Nazwisko` FROM `server_agreement_and_balance` JOIN `uzytkownicy` ON `ownerID` = `idUzytkownicy` WHERE `houseID` = " + req.body.houseID + " AND `wynajmujacyID` = "+ req.body.userID +" ; ";  
  let agreInfoH;
  let visSelectH = "SELECT `wizytacje`.`idWizyt`,  `wizytacje`.`Nazwa`,  `wizytacje`.`Tresc`, date_format(`wizytacje`.`Data_Wizytacji`, '%T %M %d %Y ') AS `Data_Wizytacji`, DATE_FORMAT(`wizytacje`.`Data_Dodania`, '%M %d %Y') AS `Data_Dodania`, `wizytacje`.`houseID` FROM `wizytacje` WHERE `houseID` = "+ req.body.houseID +" ;";  
  let visInfoH;
  let damageSelectAsync = "Select `server_damage_houseid`.`*` From `server_damage_houseid` where `houseID` =" + req.body.houseID + ";";
  let damageInfoH;


  [agreInfoH, rows] = await promisePool.query(agreSelectH);
  [visInfoH, rows] = await promisePool.query(visSelectH);
  [damageInfoH, rows] = await promisePool.query(damageSelectAsync); 
  
  res.json({ agreement: agreInfoH, visits: visInfoH, damage: damageInfoH});
   
});

router.post('/fetchAllOneAgreement', jsonParser ,async  (req, res) => {

  let paySelect = "SELECT `oplaty`.`idOplaty`,  `oplaty`.`Nazwa`,  `oplaty`.`Kwota`, DATE_FORMAT(`oplaty`.`Data_Dodania`, '%M %d %Y') AS `Data_Dodania` ,  `oplaty`.`Kwota_Zaplacona`, DATE_FORMAT(`oplaty`.`Data_Zaplacenia`, '%M %d %Y') AS `Data_Zaplacenia`,  `oplaty`.`agreementID` FROM  `oplaty` where `agreementID` = "+ req.body.agreID +" ;";  
  let payInfo;
  let mess = "Select * From `server_message` WHERE `agreementID` = "+ req.body.agreID +";";
  let messInfo;
  let mesRead = "call Message_Read("+ req.body.agreID +", "+ req.body.userID +");";     
      
      
          
      [messInfo, rows] = await promisePool.query(mess);
      await promisePool.query(mesRead); 
  
  
  [payInfo, rows] = await promisePool.query(paySelect);
  
  res.json({  payment: payInfo, message:messInfo});
   
});




module.exports = router;









